<?php 

session_start();
require_once __DIR__ . '/vendor/autoload.php';

require 'facebook_details.php';

$helper = $fb->getRedirectLoginHelper();
$permissions = ['email', 'user_likes']; // optional
$loginUrl = $helper->getLoginUrl('http://localhost:8000/login_callback.php', $permissions);

echo '<a href="' . $loginUrl . '">Log in with Facebook!</a>';