<?php 

session_start();


require 'facebook_details.php';


$helper = $fb->getRedirectLoginHelper();

try {
  $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

if (isset($accessToken)) {
  // Logged in!


$oAuth2Client = $fb->getOAuth2Client();

$tokenMetadata = $oAuth2Client->debugToken($accessToken);


if (! $accessToken->isLongLived()) {
  // Exchanges a short-lived access token for a long-lived one
  try {
    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
  } catch (Facebook\Exceptions\FacebookSDKException $e) {
    echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
    exit;
  }

  echo '<h3>Long-lived</h3>';
  var_dump($accessToken->getValue());
}

  //if token is about to expire; we renew it

  $dif = $tokenMetadata->getExpiresAt()->diff(new DateTime());
  $dif = $dif->days;

  if ($dif < 2){
    try{
      // get code from access token
      $codeFromToken = $oAuth2Client->getCodeFromLongLivedAccessToken($accessToken);
    }

    catch(FacebookSDKException $e){
      echo 'Error getting a new long-lived access token: ' . $e->getMessage();
      exit;
    }

    try{
      $accessToken = $oAuth2Client->getAccessTokenFromCode($codeFromToken);
    }

    catch (FacebookSDKException $e){
      echo 'Error getting a long lived from code: ' . $e->getMessage();
      exit;
    }
    echo 'code'.$accessToken;
  }


	$_SESSION['facebook_access_token'] = (string) $accessToken;



	
	header('Location: token_checker.php');

}